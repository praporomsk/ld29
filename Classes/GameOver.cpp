//
//  GameOver.cpp
//  ld29
//
//  Created by Roman on 29.04.14.
//
//

#include "GameOver.h"
#include "GameScene.h"

USING_NS_CC;
using namespace cocos2d::extension;

Scene* GameOver::createScene()
{
    auto scene = Scene::create();
    auto layer = GameOver::create();
    scene->addChild(layer);
    return scene;
}

GameOver::GameOver()
{
    
}

GameOver::~GameOver()
{
    
}

bool GameOver::init()
{
    if (!Layer::init())
        return false;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    auto sprite = Sprite::create("game_over.png");
    sprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    addChild(sprite, 0);
    sprite->getTexture()->setAliasTexParameters();
    sprite->setScale(2);
    
    auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(GameOver::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(GameOver::onKeyReleased, this);
    
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void GameOver::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_R) {
		Director::getInstance()->replaceScene(GameScene::scene());
	}
	
	//log("Key with keycode %d pressed", keyCode);
}

void GameOver::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{

}

