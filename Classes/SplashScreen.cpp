//
//  SplashScreen.cpp
//  templategame
//
//  Created by Roman on 16.03.14.
//
//

#include "SplashScreen.h"
#include "GameScene.h"
#include "Intro.h"

USING_NS_CC;
using namespace cocos2d::extension;

Scene* SplashScreen::createScene()
{
    auto scene = Scene::create();
    auto layer = SplashScreen::create();
    scene->addChild(layer);
    return scene;
}

SplashScreen::SplashScreen()
{
    
}

SplashScreen::~SplashScreen()
{
    
}

bool SplashScreen::init()
{
    if (!Layer::init())
        return false;

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(SplashScreen::onKeyPressed, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("splash.plist");
    
    
    auto sprite = Sprite::createWithSpriteFrameName("splash_1.png");
    sprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    addChild(sprite, 0);
    sprite->getTexture()->setAliasTexParameters();
    sprite->setScale(2);

    Vector<cocos2d::AnimationFrame *> spriteFrames;
    
    for(int i = 2; i < 5; i++)
    {
        static char buff[64];
        sprintf(buff, "splash_%d.png", i);
        ValueMap userInfo;
        AnimationFrame* animFrame = AnimationFrame::create(cache->getSpriteFrameByName(buff), 0.2, userInfo);
        spriteFrames.pushBack(animFrame);
    }
    
    sprite->runAction(RepeatForever::create(Animate::create(Animation::create(spriteFrames, 1))));
    

    auto wc = Sprite::create("wc.png");
    wc->setPosition(Point(visibleSize.width/2 + origin.x - 20, visibleSize.height/2 + origin.y));
    sprite->getTexture()->setAliasTexParameters();
    sprite->setScale(2);
    addChild(wc, 1);
    
    auto play = Sprite::create("play.png");
    play->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height * 0.15));
    addChild(play, 1);
    
    setTouchEnabled(true);
    return true;
}

void SplashScreen::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_SPACE){
		skip();
	}
}

void SplashScreen::onTouchesBegan(const std::vector<Touch *> &touches, cocos2d::Event *unused_event)
{
	skip();
}

void SplashScreen::skip()
{
	Director::getInstance()->replaceScene(Intro::createScene());
}

