//
//  ScreenManager.h
//  HelloCpp
//
//  Created by Roman on 26.04.14.
//
//

#ifndef __HelloCpp__ScreenManager__
#define __HelloCpp__ScreenManager__

#include "cocos2d.h"
#include "GameScene.h"
#include "HUDLayer.h"

class ScreenManager : public cocos2d::CCObject
{

public:
    GameScene* gameScene;
	HUDLayer* hud;
    
    ScreenManager();
	static ScreenManager& getInstance()
	{
		static ScreenManager instance_;
		return instance_;
	}
    
    
};
#endif /* defined(__HelloCpp__ScreenManager__) */
