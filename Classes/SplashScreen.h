//
//  SplashScreen.h
//  templategame
//
//  Created by Roman on 16.03.14.
//
//

#ifndef __templategame__SplashScreen__
#define __templategame__SplashScreen__

#include "cocos2d.h"
#include "cocos-ext.h"

class SplashScreen : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    SplashScreen();
    ~SplashScreen();

	void skip();
    
    CREATE_FUNC(SplashScreen);
    void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *unused_event);
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
};
#endif /* defined(__templategame__SplashScreen__) */
