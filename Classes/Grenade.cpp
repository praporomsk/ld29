//
//  Grenade.cpp
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#include "Grenade.h"
#include "ScreenManager.h"

USING_NS_CC;

Grenade::Grenade()
{
    light_ = NULL;
    body_ = NULL;
}


Grenade::~Grenade()
{
    
}

bool Grenade::init(b2World* world, const Point& pos)
{
    if(!Sprite::initWithFile("grenade.png"))
        return false;
    
    getTexture()->setAliasTexParameters();
    setScale(GLOBAL_SCALE);
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    
    body_ = world->CreateBody(&bodyDef);
    
    // Define another box shape for our dynamic body.
    
    b2PolygonShape box;
    
    box.SetAsBox((boundingBox().size.width/8.0)  / PTM_RATIO, (boundingBox().size.height/2.0) / PTM_RATIO);
    //fixtureDef.shape = &box;
    
    //    b2CircleShape *circleShape = new b2CircleShape();
    //    circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO) * getScale();
    
    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &box;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.2f;
    fixtureDef.restitution = 0.1f;
    fixtureDef.filter.categoryBits = CATEGORY_SHELL;
    fixtureDef.filter.maskBits = MASK_SHELL;
    
    body_->CreateFixture(&fixtureDef);
    body_->SetUserData(this);
    
    light_ = new LightSource(pos, LS_GRENADE);
    ScreenManager::getInstance().gameScene->getDark()->addLightSource(light_);
    
    scheduleOnce(schedule_selector(Grenade::kill), 5.0);
    //scheduleUpdate();
    return true;
}



void Grenade::setPosition(const cocos2d::Point &pos)
{
    Sprite::setPosition(pos);
    light_->setPosition(pos);
}

//void Shell::update(float delta)
//{
//    angle_ -= angleDelta_;
//    body_->SetTransform(body_->GetPosition(), angle_);
//}

void Grenade::kill(float dt)
{
    ScreenManager::getInstance().gameScene->getDark()->removeLightSource(light_);
    setTag(NeedDestroyBody);
}