//
//  Bullet.cpp
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#include "Bullet.h"
#include "ScreenManager.h"

USING_NS_CC;

Bullet::Bullet()
{
    body_ = NULL;
    light_= NULL;
    streak = NULL;
}


Bullet::~Bullet()
{
    
}

void Bullet::setActive(bool active)
{
    active_ = active;
}

bool Bullet::init(b2World* world, const Point& pos)
{
    if(!Sprite::initWithFile("bullet.png"))
        return false;
    
    active_ = true;
    setTag(BodyBullet);
    getTexture()->setAliasTexParameters();
    setScale(GLOBAL_SCALE * 2);
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    
    body_ = world->CreateBody(&bodyDef);
    
    // Define another box shape for our dynamic body.
    
    b2PolygonShape box;
    
    box.SetAsBox((boundingBox().size.width/2.0)  / PTM_RATIO, (boundingBox().size.height/2.0) / PTM_RATIO);
    //fixtureDef.shape = &box;
    
    //    b2CircleShape *circleShape = new b2CircleShape();
    //    circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO) * getScale();
    
    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &box;
    fixtureDef.density = 10.0f;
    fixtureDef.friction = 0.2f;
    fixtureDef.restitution = 0.1f;
    fixtureDef.filter.categoryBits = CATEGORY_BULLET;
    fixtureDef.filter.maskBits = MASK_BULLET;
    
    body_->CreateFixture(&fixtureDef);
    body_->SetUserData(this);
    body_->SetBullet(true);
    
    

    
    //body_->SetAngularVelocity(30);

    light_ = new LightSource(pos, LS_BULLET);
    ScreenManager::getInstance().gameScene->getDark()->addLightSource(light_);
    
    scheduleOnce(schedule_selector(Bullet::kill), 2);
    //scheduleUpdate();
    return true;
}

b2Body* Bullet::body()
{
    return body_;
}

void Bullet::setPosition(const cocos2d::Point &pos)
{
    Sprite::setPosition(pos);
    light_->setPosition(pos);
    if (streak)
        streak->setPosition(Point(pos.x * GLOBAL_SCALE * 4, pos.y * GLOBAL_SCALE * 4));
}


//void Bullet::update(float delta)
//{
////    angle_ -= angleDelta_;
////    body_->SetTransform(body_->GetPosition(), angle_);
//}

void Bullet::kill(float dt)
{
    ScreenManager::getInstance().gameScene->getDark()->removeLightSource(light_);
    setTag(NeedDestroyBody);
    streak->removeFromParentAndCleanup(true);
    streak = NULL;
}