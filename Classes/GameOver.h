//
//  GameOver.h
//  ld29
//
//  Created by Roman on 29.04.14.
//
//

#ifndef __ld29__GameOver__
#define __ld29__GameOver__

#include "cocos2d.h"
#include "cocos-ext.h"

class GameOver : public cocos2d::Layer
{
public:
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

    static cocos2d::Scene* createScene();
    virtual bool init();
    GameOver();
    ~GameOver();
    
    CREATE_FUNC(GameOver);
};
#endif /* defined(__ld29__GameOver__) */
