#include "LightSource.h"
#include "ScreenManager.h"

USING_NS_CC;

LightSource::LightSource(CCPoint pos, LightType type)
{
	position_ = pos;
	type_ = type;
}

void LightSource::kill(float dt)
{
	if (ScreenManager::getInstance().gameScene)
		ScreenManager::getInstance().gameScene->getDark()->removeLightSource(this);
}