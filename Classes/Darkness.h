#ifndef __DARKNESS__
#define __DARKNESS__

#include "cocos2d.h"
#include "LightSource.h"
#include "Player.h"

class Darkness : public cocos2d::Node
{
	cocos2d::Texture2D* texture_;
	cocos2d::RenderTexture* render;
	cocos2d::Sprite* light;
	cocos2d::Size darkSize;
	cocos2d::Size winSize;

	cocos2d::__Array* lights;

	Player* player;

public:
	Darkness(int w, int h);
	virtual ~Darkness();

	void addLightSource(LightSource* ls);
	void removeLightSource(LightSource* ls);

	virtual void visit(cocos2d::Renderer* renderer, const kmMat4 &parentTransform, bool parentTransformUpdated);
};

#endif /* defined(__DARKNESS__) */