//
//  WinScene.cpp
//  ld29
//
//  Created by Roman on 29.04.14.
//
//

#include "WinScene.h"
#include "GameScene.h"

USING_NS_CC;
using namespace cocos2d::extension;

Scene* WinScene::createScene()
{
    auto scene = Scene::create();
    auto layer = WinScene::create();
    scene->addChild(layer);
    return scene;
}

bool WinScene::init()
{
    if (!Layer::init())
        return false;
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    auto sprite = Sprite::create("the_end.png");
    sprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    addChild(sprite, 0);
    sprite->getTexture()->setAliasTexParameters();
    sprite->setScale(2);
    
    auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(WinScene::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(WinScene::onKeyReleased, this);
    
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    return true;
}

void WinScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_R) {
		Director::getInstance()->replaceScene(GameScene::scene());
	}
	
	//log("Key with keycode %d pressed", keyCode);
}

void WinScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    
}