//
//  Platform.cpp
//  Lost Sunrise
//
//  Created by Roman on 5/8/13.
//
//

#include "Platform.h"
#include "Constants.h"
#include "MapHelper.h"

Platform::Platform()
{
    
}

Platform::~Platform()
{

}

bool Platform::init(b2World *world, ValueMap properties)
{
    
    Value typeValue = properties["polylinePoints"];
    
    ValueVector pointsVector = typeValue.asValueVector();
    

    PointArray *points = new PointArray();
    points->initWithCapacity(1);
    points->autorelease();
    points = MapHelper::parseDictionary(properties);
//
    if (CCNode::init())
    {
        //setTag(ObjectTypePlatform);
        b2BodyDef bodyDef;
        bodyDef.type = b2_staticBody;
        bodyDef.position.Set(0, 0);
        body = world->CreateBody(&bodyDef);

        b2EdgeShape groundBox;
        for (int i = 0; i < points->count(); i++)
        {
            Point pos = points->getControlPointAtIndex(i);
            Point nextPos = points->getControlPointAtIndex(i+1);
			groundBox.Set(b2Vec2(pos.x * GLOBAL_SCALE / PTM_RATIO, pos.y * GLOBAL_SCALE / PTM_RATIO), b2Vec2(nextPos.x * GLOBAL_SCALE / PTM_RATIO, nextPos.y * GLOBAL_SCALE / PTM_RATIO));
            body->CreateFixture(&groundBox,0);
            
            b2Fixture *fixture = body->GetFixtureList();
            b2Filter filter;
            filter.categoryBits = CATEGORY_PLATFORM;
            filter.maskBits = MASK_PLATFORM;
            fixture->SetFilterData(filter);
        }
    
        body->SetUserData(this);
        return true;
    }
    return false;
}