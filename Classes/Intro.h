#ifndef __intro__
#define __intro__

#include "cocos2d.h"
#include "cocos-ext.h"

class Intro : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	Intro();
	~Intro();

	void skip();

	CREATE_FUNC(Intro);
	void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *unused_event);

	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
};
#endif /* defined(__intro__) */