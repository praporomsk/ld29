//
//  Player.cpp
//  HelloCpp
//
//  Created by Roman on 26.04.14.
//
//

#include "Player.h"
#include "Shell.h"
#include "Bullet.h"
#include "Grenade.h"
#include "ScreenManager.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Player::Player()
{
    body_ = NULL;
    shootAnim_ = NULL;
    curAction_ = NULL;
    stepCount_ = 0;
    world_ = NULL;
    
    pressRight_ = false;
    pressLeft_ = false;
    light_ = NULL;

	reset();
}


Player::~Player()
{
    
    CC_SAFE_RELEASE(shootAnim_);
}

PlayerState Player::getState()
{
	return state_;
}

void Player::setIdle()
{
    setState(PS_IDLE);
}

void Player::setState(PlayerState state)
{
    if (state == state_)
        return;
    
    state_ = state;
    
    if (state_ != PS_ZIPPO)
        light_->setType(LS_SIGA);
    
    
    if (state_ == PS_MOVE_LEFT) {
        
    }else if (state_ == PS_MOVE_RIGHT){
   
    }else if (state_ == PS_SHOOT){
        CallFunc* setInde = CallFunc::create( CC_CALLBACK_0(Player::setIdle,this));
        runAction(Sequence::createWithTwoActions(shootAnim_, setInde));
    }else if(state_ == PS_IDLE){
        setSpriteFrame("cahar_norm.png");
    }else if (state_ == PS_ZIPPO){
        setSpriteFrame("zippo_anim1.png");
    }
    

    
}

bool Player::isFly()
{
    b2Vec2 vel = body_->GetLinearVelocity();

	if (vel.y < 0.01 && vel.y > -0.01)
        return false;
    
    return true;
}

void Player::reset()
{
	bullets = 6;
	zippos = 8;
	grenades = 3;

	ScreenManager::getInstance().hud->setGun(bullets);
	ScreenManager::getInstance().hud->setZippo(zippos);
	ScreenManager::getInstance().hud->setGrenade(grenades);
}

void Player::shoot()
{
	if (!bullets)
		return;

	--bullets;

	ScreenManager::getInstance().hud->setGun(bullets);

    setState(PS_SHOOT);

    Shell* shell = new Shell();

    Point pos = Point(getPositionX() + 20 * getScaleX(), getPositionY());

	LightSource* src = new LightSource(pos, LS_SHOT);
	ScreenManager::getInstance().gameScene->getDark()->addLightSource(src);

	CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(LightSource::kill), src, 0.2f, 0, 0.2f, false);

	shell->init(world_, pos);
	shell->autorelease();
	getParent()->addChild(shell);
    //addChild(player_);

	for (int i = 0; i < 4; ++i)
	{
		Bullet* bullet = new Bullet();
		bullet->init(world_, pos);
		getParent()->addChild(bullet);


		if (getScaleX() > 0) {
			bullet->body()->SetLinearVelocity(b2Vec2(50, (i-1)*3));
		}
		else{
			bullet->body()->SetLinearVelocity(b2Vec2(-50, (i-1)*3));
		}

		MotionStreak* str = MotionStreak::create(0.3f, 1.0f, 10.0f, Color3B(255, 255, 255), "bullet.png");
		str->setScale(0.25 / GLOBAL_SCALE);
		getParent()->addChild(str, 1010);
		bullet->streak = str;
	}
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("shoot.wav");

}

bool Player::init(b2World* world)
{
    world_ = world;
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("character.plist");
    

    Point pos = Point(100, 200);
    
    if(!Sprite::initWithSpriteFrameName("char_walk5.png"))
        return false;

    setAnchorPoint(Point(0.25, 0.5));
	light_ = new LightSource(pos, LS_SIGA);
    setTag(BodyPlayer);
    getTexture()->setAliasTexParameters();
    setScale(GLOBAL_SCALE);
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    
    body_ = world->CreateBody(&bodyDef);
    
    // Define another box shape for our dynamic body.

    b2PolygonShape box;

    box.SetAsBox((boundingBox().size.width/5.0)  / PTM_RATIO, (boundingBox().size.height/2.0) / PTM_RATIO);
    //fixtureDef.shape = &box;
    
//    b2CircleShape *circleShape = new b2CircleShape();
//    circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO) * getScale();
    
    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &box;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.1f;
    fixtureDef.restitution = 0.0f;
    fixtureDef.filter.categoryBits = CATEGORY_PLAYER;
    fixtureDef.filter.maskBits = MASK_PLAYER;
    
    body_->CreateFixture(&fixtureDef);
    body_->SetUserData(this);
    
    body_->SetFixedRotation(true);
//    delete circleShape;
//    circleShape = NULL;

    Vector<cocos2d::AnimationFrame *> spriteFrames;
   
    for(int i = 0; i < 2; i++)
    {
        static char buff[64];
        sprintf(buff, "char_shoot%d.png", i);
        ValueMap userInfo;
        AnimationFrame* animFrame = AnimationFrame::create(cache->getSpriteFrameByName(buff), 0.2, userInfo);
        spriteFrames.pushBack(animFrame);
    }

    shootAnim_ = Animate::create(Animation::create(spriteFrames, 0.6));
    CC_SAFE_RETAIN(shootAnim_);
    
    scheduleUpdate();
    return true;
}

int Player::getGID()
{
    Point pos = getPosition();
    
    TMXTiledMap* map = ScreenManager::getInstance().gameScene->getMap();
    TMXLayer* layer = map->getLayer("Ground");
    
    Point tilePos  = Point((pos.x / GLOBAL_SCALE) / map->getTileSize().width,
                           map->getMapSize().height - (pos.y / GLOBAL_SCALE)/ map->getTileSize().height);
    tilePos = Point(roundf(tilePos.x - 0.5), roundf(tilePos.y + 0.2));

    int gid = layer->getTileGIDAt(tilePos);
    //CCLOG("gid = %d",gid);
    return gid;
}


void Player::update(float delta)
{
    
    Sprite::update(delta);
    if ( pressRight_) {
        if (!isFly())
            setState(PS_MOVE_RIGHT);
    
        body()->SetLinearVelocity(b2Vec2(1.5, body()->GetLinearVelocity().y));
    }else if (pressLeft_){
        if (!isFly())
         setState(PS_MOVE_LEFT);
        
        body()->SetLinearVelocity(b2Vec2(-1.5, body()->GetLinearVelocity().y));
    }else if (pressUp_ && getGID() == stairsID){
        
        b2Vec2 pos = body_->GetLinearVelocity();
        
        body_->SetLinearVelocity(b2Vec2(pos.x, 5));
        //SetTransform(b2Vec2(pos.x, pos.y + 0.1), 0);
    }
    
    
    
}

void Player::setPosition(const cocos2d::Point &pos)
{
    Sprite::setPosition(pos);

    light_->setPosition(Point(pos.x + 14, pos.y + 30));
//    if (!isFly()) {
//        setState(<#PlayerState state#>)
//    }
    
    if (state_ == PS_MOVE_LEFT ||
        state_ == PS_MOVE_RIGHT){
        float stepDis = 3;
        if(lastStepPosX + stepDis < pos.x){
            lastStepPosX = pos.x;
            stepCount_++;
            if(stepCount_ == 5)
                stepCount_ = 0;
            
            updateStep();
        }
        
        if(lastStepPosX - stepDis > pos.x)
        {
            lastStepPosX = pos.x;
            stepCount_--;
            if(stepCount_ == -1)
                stepCount_ = 4;
            
            updateStep();
        }
    }
}

void Player::updateStep()
{
    static char buff[64];
    sprintf(buff, "char_walk%d.png", stepCount_ + 1);
    setSpriteFrame(buff);
    
    if (stepCount_ == 4)
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("step.wav");
}


b2Body* Player::body()
{
    return body_;
}

void Player::stop()
{
    body_->SetLinearVelocity(b2Vec2(0, 0));
}

void Player::onLeft(bool isPush)
{
    pressLeft_ = isPush;
    setScaleX(-GLOBAL_SCALE);
}
void Player::onRight(bool isPush)
{
    pressRight_ = isPush;
    setScaleX(GLOBAL_SCALE);
}

void Player::onJump(bool isPush)
{
    pressUp_ = isPush;
    
    if (getGID() == stairsID)
        return;
    
	if (isPush && !isFly())
	{
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("jump.wav");
		b2Vec2 velocity = body()->GetLinearVelocity();
		body()->SetLinearVelocity(b2Vec2(velocity.x, 10));
        setState(PS_JUMP);
	}
}

void Player::onZippo()
{
	if (!zippos)
		return;

	//--zippos;

	ScreenManager::getInstance().hud->setZippo(zippos);

    setState(PS_ZIPPO);
    light_->setType(LS_BIG);
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("zippo_sound.wav");
}

void Player::onGrenade()
{
	if (!grenades)
		return;

	--grenades;

	ScreenManager::getInstance().hud->setGrenade(grenades);

    Point pos = getPosition();
    Grenade* grenade = new Grenade();
    grenade->init(world_, Point(pos.x, pos.y + 50));
    grenade->autorelease();
    getParent()->addChild(grenade);
    if (getScaleX() > 0) {
        grenade->body()->SetAngularVelocity(10 + randomNumber(15));
        grenade->body()->ApplyForceToCenter(b2Vec2(300, 200), false);
    }else{
        grenade->body()->SetAngularVelocity(-10 - randomNumber(15));
        grenade->body()->ApplyForceToCenter(b2Vec2(-300, 200), false);
    }

    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("shashka.wav");
    
}