//
//  MapHelper.h
//  Rabbit
//
//  Created by Roman on 4/18/13.
//
//

#ifndef __Rabbit__MapHelper__
#define __Rabbit__MapHelper__

#include <iostream>
#include "cocos2d.h"
using namespace cocos2d;

class MapHelper
{
public:

    static PointArray* parseDictionary(ValueMap properties);
    static Point positionWithProperties(ValueMap properties);
};

#endif /* defined(__Rabbit__MapHelper__) */
