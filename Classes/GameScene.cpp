//
//  GameScene.cpp
//  HelloCpp
//
//  Created by Roman on 26.04.14.
//
//

#include "GameScene.h"
#include "cocos-ext.h"
#include "Platform.h"
#include "ScreenManager.h"
#include "Monster.h"
#include "Bullet.h"

#include "SimpleAudioEngine.h"
#include "GameOver.h"
#include "WinScene.h"

#define ifDynamicCast(__type__,__var__,__varName__) \
	__type__ __varName__ = dynamic_cast<__type__>(__var__); \
if (__varName__)

USING_NS_CC;
using namespace extension;

Scene* GameScene::scene()
{
	// 'scene' is an autorelease object
	Scene *scene = CCScene::create();
	scene->setTag(SCENE_GAME);

	HUDLayer* hud = new HUDLayer();
	hud->init();
	hud->autorelease();
	scene->addChild(hud, 10);
	ScreenManager::getInstance().hud = hud;

	// 'layer' is an autorelease object
	GameScene *layer = GameScene::create();
	ScreenManager::getInstance().gameScene = layer;

	// add layer as a child to scene
	scene->addChild(layer, 1, 99);
    
	layer->hud_ = hud;
    
	// return the scene
	return scene;
}

GameScene::GameScene()
{
	player_ = NULL;
	world_ = NULL;
	groundBody_ = NULL;
	map_ = NULL;
    hud_ = NULL;
    tutop = NULL;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!CCLayer::init())
	{
		return false;
	}

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(GameScene::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	addPhysicsWorld();

	Size visibleSize = CCDirector::sharedDirector()->getVisibleSize();
	Point origin = CCDirector::sharedDirector()->getVisibleOrigin();


	addBackground();


    
	player_ = new Player();
	player_->init(world_);
	player_->autorelease();
	addChild(player_);

    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    cache->addSpriteFramesWithFile("enemy.plist");
    

	scheduleUpdate();

    tutop = Sprite::create("tutor.png");
    tutop->getTexture()->setAliasTexParameters();
    tutop->setScale(GLOBAL_SCALE);
    tutop->setPosition(Point(visibleSize.width * 0.5, visibleSize.height* 0.5));
    addChild(tutop, 1030);
    
	CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("theme_01.mp3", true);
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("jump.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("cool_random.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("monster_walk.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("shashka.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("shoot.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("step.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("zippo_sound.wav");

    schedule(schedule_selector(GameScene::spawnEnemy), 5);
  
	return true;
}



void GameScene::spawnEnemy(float delta)
{
    if (randomNumber(100) < 50)
        return;
    
    Size visibleSize = CCDirector::getInstance()->getVisibleSize();
    
    Monster* monster = new Monster();
	monster->init(world_, Point(-getPosition().x + visibleSize.width * 1.5, 100 + randomNumber(220)));
	monster->autorelease();
	addChild(monster);

    addChild(monster->eyes(), 1010);

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("monster_walk.wav");
}

void GameScene::onEnter()
{
	CCLayer::onEnter();

	Size s = CCDirector::sharedDirector()->getVisibleSize();
	Size mapSize = map_->getMapSize();
	Size tileSize = map_->getTileSize();
	this->runAction(CCFollow::create(player_, CCRectMake(0, 0, tileSize.width*mapSize.width*GLOBAL_SCALE, s.height*GLOBAL_SCALE)));

	dark_ = new Darkness(mapSize.width*tileSize.width*GLOBAL_SCALE, (mapSize.height-0.2)*tileSize.height*GLOBAL_SCALE);
	dark_->autorelease();
	addChild(dark_, 1000);//CCTextureCache::sharedTextureCache()->addImage(path.c_str());
	dark_->setAnchorPoint(ccp(0, 0));
	dark_->setPosition(ccp(0, 0));

	dark_->addLightSource(player_->light());

}

void GameScene::addPhysicsWorld()
{
	Size s = CCDirector::sharedDirector()->getVisibleSize();

	b2Vec2 gravity;
	gravity.Set(0.0f, -10.0f);
	world_ = new b2World(gravity);

	// Do we want to let bodies sleep?
	world_->SetAllowSleeping(true);

	world_->SetContinuousPhysics(true);

	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner

	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody_ = world_->CreateBody(&groundBodyDef);
    
    m_contactListener = new MyContactListener();
    world_->SetContactListener(m_contactListener);
}

void GameScene::update(float dt)
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/

	int32 velocityIterations = 8;
	int32 positionIterations = 1;

	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world_->Step(dt, velocityIterations, positionIterations);

	std::vector<b2Body*> trash;
    
	for (b2Body* b = world_->GetBodyList(); b; b = b->GetNext())
	{
		if (b->GetUserData() != NULL) {
			//Synchronize the AtlasSprites position and rotation with the corresponding body
			Sprite* myActor = (Sprite*)b->GetUserData();
			if (myActor->getTag() == NeedDestroyBody)
			{
				trash.push_back(b);
				myActor->removeFromParentAndCleanup(true);
			}
			else
			{
				myActor->setPosition(Point(b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO));
				myActor->setRotation(-1 * CC_RADIANS_TO_DEGREES(b->GetAngle()));
			}
		}
	}
    
	for (std::vector<b2Body*>::iterator it = trash.begin(); it != trash.end(); ++it)
		world_->DestroyBody(*it);
    
    std::vector<MyContact>::iterator pos;
    for(pos = m_contactListener->_contacts.begin(); pos != m_contactListener->_contacts.end(); ++pos) {
        MyContact contact = *pos;

        b2Body *bodyA = contact.fixtureA->GetBody();
        b2Body *bodyB = contact.fixtureB->GetBody();
        if (bodyA->GetUserData() != NULL && bodyB->GetUserData() != NULL) {
 
            Node *nodeA = (Node*) bodyA->GetUserData();
            Node *nodeB = (Node*) bodyB->GetUserData();
            
            if ((nodeA->getTag() == BodyBullet && nodeB->getTag() == BodyEnemy) ||
                (nodeA->getTag() == BodyEnemy && nodeB->getTag() == BodyBullet))
            {
                Bullet* bullet = NULL;
                Monster* monster = NULL;
                if (nodeA->getTag() == BodyBullet) {
                    bullet = (Bullet*)nodeA;
                    if (!bullet->active())
                        return;
                    
                    monster = (Monster*) nodeB;
                }else{
                    bullet = (Bullet*)nodeB;
                    if (!bullet->active())
                        return;
                    
                    monster = (Monster*) nodeA;
                }
                
                monster->kill();
                bullet->kill(0);
                
            }else if ((nodeA->getTag() == BodyEnemy && nodeB->getTag() == BodyPlayer) ||
                      (nodeA->getTag() == BodyPlayer && nodeB->getTag() == BodyEnemy)){
                gameOver();
            }else if ((nodeA->getTag() == BodyPlayer && nodeB->getTag() == BodyEnd) ||
                      (nodeA->getTag() == BodyEnd && nodeB->getTag() == BodyPlayer)){
                win();
            }
            
            if (nodeA->getTag() == BodyBullet)
            {
                Bullet* bullet = (Bullet*) nodeA;
                bullet->setActive(false);
            }
            if (nodeB->getTag() == BodyBullet)
            {
                Bullet* bullet = (Bullet*) nodeB;
                bullet->setActive(false);
            }
        }
    }
    if (player_->getPositionY() < 0)  
        gameOver();
    

}

void GameScene::win()
{
     Director::getInstance()->replaceScene(WinScene::createScene());
}

void GameScene::gameOver()
{
    CCLOG("gameOver");
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("cool_random.wav");
    Director::getInstance()->replaceScene(GameOver::createScene());
}
void GameScene::addBackground()
{
    SpriteBatchNode* groundBatch = SpriteBatchNode::create("undergraund_loop.png");
    addChild(groundBatch);
    float h;
    
    for (int i = 0; i < 15; i++)
    {
        Sprite* ground = Sprite::create("undergraund_loop.png");
        ground->setAnchorPoint(Point(0, 0));
        groundBatch->addChild(ground);
        ground->setPosition(Point(i * ground->getBoundingBox().size.width, 0));
        
        h = ground->getBoundingBox().size.height;
    }
    
    SpriteBatchNode* forestBatch = SpriteBatchNode::create("forest_loop.png");
    addChild(forestBatch);
    for (int i = 0; i < 10; i++)
    {
        Sprite* forest = Sprite::create("forest_loop.png");
        forest->setAnchorPoint(Point(0, 0));
        forestBatch->addChild(forest);
        forest->setPosition(Point(i * forest->getBoundingBox().size.width, h));
    }
    
	map_ = CCTMXTiledMap::create("level1.tmx");
	map_->setScale(GLOBAL_SCALE);
	addChild(map_);

	parseTileMap();
}

void GameScene::parseTileMap()
{
	Vector<TMXObjectGroup*> groups = map_->getObjectGroups();


	for (int i = 0; i < groups.size(); i++)
	{
		TMXObjectGroup* group = groups.at(i);
		ValueVector objects = group->getObjects();

		for (int j = 0; j < objects.size(); j++)
		{
			Value value = objects.at(j);
			addObject(value);
		}
	}
}

void GameScene::addObject(Value properties)
{
	ValueMap valueMap = properties.asValueMap();

	Value typeValue = valueMap["type"];

	std::string type = typeValue.asString();

	CCLOG("type =%s", type.c_str());

	if (type.compare("Platform") == 0)
	{
		Platform *platform = new Platform();
		platform->init(world_, valueMap);
		platform->autorelease();
		addChild(platform);
	}else if (type.compare("end") == 0){
        Platform *platform = new Platform();
		platform->init(world_, valueMap);
		platform->autorelease();
        platform->setTag(BodyEnd);
		addChild(platform);
    }

}


void GameScene::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
    
    if (tutop != NULL) {
        tutop->removeFromParentAndCleanup(true);
        tutop=NULL;
    }
	if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW) {
		player_->onRight(true);
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		player_->onLeft(true);
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_UP_ARROW){
		player_->onJump(true);
	}else if (keyCode == EventKeyboard::KeyCode::KEY_SPACE){
        player_->shoot();
    }else if (keyCode == EventKeyboard::KeyCode::KEY_Z){
        player_->onZippo();
    }else if (keyCode == EventKeyboard::KeyCode::KEY_X){
        player_->onGrenade();
    }
	log("Key with keycode %d pressed", keyCode);
}

void GameScene::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW) {
		player_->onRight(false);
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW){
		player_->onLeft(false);
	}
	else if (keyCode == EventKeyboard::KeyCode::KEY_UP_ARROW){
		player_->onJump(false);
	}
	log("Key with keycode %d released", keyCode);
}


