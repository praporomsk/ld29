//
//  Constants.h
//  HelloCpp
//
//  Created by Roman on 26.04.14.
//
//

#ifndef HelloCpp_Constants_h
#define HelloCpp_Constants_h

#define MAP_HEIGHT 290
#define PTM_RATIO 32
#define GLOBAL_SCALE 2.0

#define stairsID 4
#define winID 18

inline float randomNumber(float range)
{
    return((float)(rand() * range)/RAND_MAX);
}

typedef enum BodyTag_t  {
    BodyTagNone,
    BodyPlayer,
    BodyBullet,
    BodyEnemy,
    BodyEnd,
    NeedDestroyBody,
} BodyTag;


typedef enum SceneTag_t  {
    SCENE_NONE,
    SCENE_GAME,
} SceneTag;

typedef enum PlayerState_t  {
	PS_MOVE_LEFT,
	PS_MOVE_RIGHT,
	PS_JUMP,
	PS_IDLE,
    PS_SHOOT,
    PS_ZIPPO,
} PlayerState;

//https://gitorious.org/qml-box2d/qml-box2d/source/47ab19e6eae01ce5506c8348588620817beef17e:box2dfixture.h
/*
 enum CategoryFlag {Category1 = 0x0001, Category2 = 0x0002, Category3 = 0x0004, Category4 = 0x0008,
 Category5 = 0x0010, Category6 = 0x0020, Category7 = 0x0040, Category8 = 0x0080,
 Category9 = 0x0100, Category10 = 0x0200, Category11 = 0x0400, Category12 = 0x0800,
 Category13 = 0x1000, Category14 = 0x2000, Category15 = 0x4000, Category16 = 0x8000,
 All = 0xFFFF, None=0x0000};
 */

enum _entityCategory {
    CATEGORY_PLAYER =          0x0001,
    CATEGORY_ENEMY =       0x0002,
    CATEGORY_PLATFORM =     0x0004,
    CATEGORY_SHELL =      0x0008,
    CATEGORY_BULLET =    0x0100,
};

enum _entityMask {
    MASK_PLAYER = CATEGORY_PLATFORM | CATEGORY_ENEMY,
    MASK_ENEMY = CATEGORY_BULLET | CATEGORY_PLAYER,
    MASK_PLATFORM = CATEGORY_PLAYER | CATEGORY_SHELL | CATEGORY_BULLET,
    MASK_SHELL = CATEGORY_PLATFORM,
    MASK_BULLET = CATEGORY_PLATFORM | CATEGORY_ENEMY,
};

#endif
