#ifndef __LIGHT_SOURCE__
#define __LIGHT_SOURCE__

#include "cocos2d.h"

typedef enum {
	LS_SMALL = 0,
    LS_SIGA,
    LS_BULLET,
	LS_SHOT,
    LS_GRENADE,
	LS_BIG,
} LightType;

class LightSource : public cocos2d::CCObject
{
	cocos2d::CCPoint position_;
	LightType type_;

public:
	LightSource(cocos2d::CCPoint pos, LightType type);

	cocos2d::CCPoint getPosition() { return position_; }
	LightType getType() { return type_; }

	void setPosition(cocos2d::CCPoint pos) { position_ = pos; }
	void setType(LightType type) { type_ = type; }

	void kill(float dt);
};

#endif