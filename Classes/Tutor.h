//
//  Tutor.h
//  ld29
//
//  Created by Roman on 29.04.14.
//
//

#ifndef __ld29__Tutor__
#define __ld29__Tutor__

#include "cocos2d.h"
#include "cocos-ext.h"

class Tutor : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    
	void skip();

    CREATE_FUNC(Tutor);
    void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *unused_event);
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
};
#endif /* defined(__ld29__Tutor__) */
