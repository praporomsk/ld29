//
//  SplashScreen.cpp
//  templategame
//
//  Created by Roman on 16.03.14.
//
//

#include "Intro.h"
#include "GameScene.h"

USING_NS_CC;
using namespace cocos2d::extension;

Scene* Intro::createScene()
{
	auto scene = Scene::create();
	auto layer = Intro::create();
	scene->addChild(layer);
	return scene;
}

Intro::Intro()
{

}

Intro::~Intro()
{

}

bool Intro::init()
{
	if (!Layer::init())
		return false;

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(Intro::onKeyPressed, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	SpriteFrameCache* cache = SpriteFrameCache::getInstance();
	cache->addSpriteFramesWithFile("intro.plist");

	auto sprite = Sprite::createWithSpriteFrameName("01.png");
	sprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	addChild(sprite, 0);
	sprite->getTexture()->setAliasTexParameters();
	sprite->setScale(2);

	Vector<cocos2d::AnimationFrame *> spriteFrames;

	for (int i = 2; i <= 19; i++)
	{
		static char buff[64];
		sprintf(buff, "%.2d.png", i);
		ValueMap userInfo;
		AnimationFrame* animFrame = AnimationFrame::create(cache->getSpriteFrameByName(buff), 0.3, userInfo);
		spriteFrames.pushBack(animFrame);
	}

	sprite->runAction(Sequence::create(
		Animate::create(Animation::create(spriteFrames, 1)), CallFunc::create(this, callfunc_selector(Intro::skip)), NULL));
	/*
	auto play = Sprite::create("play.png");
	play->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height * 0.15));
	addChild(play, 1);
	*/
	setTouchEnabled(true);
	return true;
}

void Intro::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_SPACE){
		skip();
	}
}


void Intro::onTouchesBegan(const std::vector<Touch *> &touches, cocos2d::Event *unused_event)
{
	skip();
}

void Intro::skip()
{
	Director::getInstance()->replaceScene(GameScene::scene());
}