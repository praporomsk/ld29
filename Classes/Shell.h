//
//  Shell.h
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#ifndef __ld29__Shell__
#define __ld29__Shell__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Constants.h"

class Shell : public cocos2d::Sprite
{
    b2Body* body_;
public:
    //void update(float delta);
    void kill(float dt);
    Shell();
    virtual ~Shell();

    bool init(b2World* world, const cocos2d::Point& pos);
    void shoot();
};
#endif /* defined(__ld29__Shell__) */
