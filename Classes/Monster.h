//
//  Monster.h
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#ifndef __ld29__Monster__
#define __ld29__Monster__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Constants.h"

class Monster : public cocos2d::Sprite
{
    b2Body* body_;
    Sprite* eyes_;
public:
    void kill();
    Sprite* eyes() { return eyes_; }
    void setPosition(const cocos2d::Point &pos);
    void update(float delta);
    Monster();
    virtual ~Monster();
    
    bool init(b2World* world, const cocos2d::Point& pos);

};

#endif /* defined(__ld29__Monster__) */
