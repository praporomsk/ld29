//
//  Tutor.cpp
//  ld29
//
//  Created by Roman on 29.04.14.
//
//

#include "Tutor.h"
#include "GameScene.h"

USING_NS_CC;
using namespace cocos2d::extension;

Scene* Tutor::createScene()
{
    auto scene = Scene::create();
    auto layer = Tutor::create();
    scene->addChild(layer);
    return scene;
}

bool Tutor::init()
{
    if (!Layer::init())
        return false;

	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(Tutor::onKeyPressed, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    auto sprite = Sprite::create("tutor.png");
    sprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    addChild(sprite, 0);
    sprite->getTexture()->setAliasTexParameters();
    sprite->setScale(2);
    
    setTouchEnabled(true);
    return true;
}

void Tutor::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	skip();
}

void Tutor::onTouchesBegan(const std::vector<Touch *> &touches, cocos2d::Event *unused_event)
{
	skip();
}

void Tutor::skip()
{
	Director::getInstance()->replaceScene(GameScene::scene());
}