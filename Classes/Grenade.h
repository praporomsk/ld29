//
//  Grenade.h
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#ifndef __ld29__Grenade__
#define __ld29__Grenade__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Constants.h"
#include "LightSource.h"

class Grenade : public cocos2d::Sprite
{
    b2Body* body_;
    LightSource* light_;
protected:
    void setPosition(const cocos2d::Point &pos);
public:
    b2Body* body() { return body_; }
    //void update(float delta);
    void kill(float dt);
    Grenade();
    virtual ~Grenade();
    
    bool init(b2World* world, const cocos2d::Point& pos);
};
#endif /* defined(__ld29__Grenade__) */
