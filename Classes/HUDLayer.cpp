//
//  HUDLayer.cpp
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#include "HUDLayer.h"
#include "ScreenManager.h"

USING_NS_CC;

#define COUNT_TAG 111

bool HUDLayer::init()
{
    if (!Layer::init())
        return false;

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("numbers.plist");

	Point countPos = Point(40, 15);
    
    Sprite* norm = Sprite::create("zippo_activ.png");
    Sprite* sel = Sprite::create("zippo_activ.png");
    Sprite* dis = Sprite::create("zippo_inactiv.png");
    zippoItem = MenuItemSprite::create(norm, sel, dis, CC_CALLBACK_0(HUDLayer::onZippo, this));
	Sprite* zCount = Sprite::create();
	zCount->setPosition(countPos);
	zCount->setRotation(90);
	zippoItem->addChild(zCount, 1, COUNT_TAG);
    
    zippoItem->setPosition(Point(120, 40));

	Sprite* gun_norm = Sprite::create("rifle_activ.png");
	Sprite* gun_sel = Sprite::create("rifle_activ.png");
	Sprite* gun_dis = Sprite::create("rifle_inactiv.png");
	gunItem = MenuItemSprite::create(gun_norm, gun_sel, gun_dis, CC_CALLBACK_0(HUDLayer::onZippo, this));
	Sprite* gCount = Sprite::create();
	gCount->setPosition(countPos);
	gunItem->addChild(gCount, 1, COUNT_TAG);

	gunItem->setPosition(Point(50, 40));

	Sprite* sh_norm = Sprite::create("shashka_activ1.png");
	Sprite* sh_sel = Sprite::create("shashka_activ1.png");
	Sprite* sh_dis = Sprite::create("shashka_inactiv1.png");
	shItem = MenuItemSprite::create(sh_norm, sh_sel, sh_dis, CC_CALLBACK_0(HUDLayer::onZippo, this));
	Sprite* sCount = Sprite::create();
	sCount->setPosition(countPos);
	shItem->addChild(sCount, 1, COUNT_TAG);

	shItem->setPosition(Point(190, 40));
	
    Menu* menu = Menu::create(zippoItem, gunItem, shItem, NULL);
    menu->setPosition(Point(0, 0));
    addChild(menu);
    

    addAmmo();
    
    return true;
}

#define maxAmmo 13

void HUDLayer::addAmmo()
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("hud.plist");
    
    Size screenSize = CCDirector::getInstance()->getVisibleSize();
    SpriteBatchNode* hudBatch = SpriteBatchNode::create("hud.png");
    addChild(hudBatch);
    
    ammoBar = Sprite::createWithSpriteFrameName("ammo_bar.png");
    ammoBar->setScale(GLOBAL_SCALE);
    ammoBar->setPosition(Point(screenSize.width * 0.3, screenSize.height * 0.9));
    hudBatch->addChild(ammoBar);
    
    for (int i = 0; i <= maxAmmo; ++i)
    {
        Sprite* ammo = Sprite::createWithSpriteFrameName("ammo.png");
        ammo->setAnchorPoint(Point(0, 0));
        ammo->setPosition(Point( 1+ i * 13, -2));
        ammoBar->addChild(ammo);
    }
    
}

void HUDLayer::setGun(int count)
{
	gunItem->setEnabled(count);

	Sprite* spr = (Sprite*)gunItem->getChildByTag(COUNT_TAG);
	char buff[64];
	sprintf(buff, "%d.png", count);
	spr->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(buff));
    
    for (int i = 0; i < ammoBar->getChildren().size(); i++)
    {
        Node* ammo = ammoBar->getChildren().at(i);
        bool visible = i < count ? true : false;
        ammo->setVisible(visible);
    }
}

void HUDLayer::setGrenade(int count)
{
	shItem->setEnabled(count);

	Sprite* spr = (Sprite*)shItem->getChildByTag(COUNT_TAG);
	char buff[64];
	sprintf(buff, "%d.png", count);
	spr->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(buff));
}

void HUDLayer::setZippo(int count)
{
	zippoItem->setEnabled(count);

	Sprite* spr = (Sprite*)zippoItem->getChildByTag(COUNT_TAG);
	char buff[64];
	sprintf(buff, "%d.png", count);
	spr->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(buff));
}

void HUDLayer::onZippo()
{
    //ScreenManager::getInstance().gameScene->getPlayer()->onZippo();
}