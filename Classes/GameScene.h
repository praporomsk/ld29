//
//  GameScene.h
//  HelloCpp
//
//  Created by Roman on 26.04.14.
//
//

#ifndef __HelloCpp__GameScene__
#define __HelloCpp__GameScene__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
//#include "GLES-Render.h"
#include "Constants.h"
#include "Player.h"
#include "Darkness.h"
#include "HUDLayer.h"
#include "MyContactListener.h"

class GameScene : public cocos2d::Layer
{
private:

    void addPhysicsWorld();
    void addBackground();
    void parseTileMap();
    void addObject(cocos2d::Value properties);
    void spawnEnemy(float delta);
    void gameOver();
    void win();
    
    b2World *world_;
    b2Body* groundBody_;
    //GLESDebugDraw *debugDraw_;
    Player* player_;
    cocos2d::TMXTiledMap* map_;
	Darkness* dark_;
    MyContactListener *m_contactListener;
    cocos2d::Sprite* tutop;
public:
    HUDLayer* hud_;
    GameScene();
    
    void update(float dt);

	Player* getPlayer() { return player_; }
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
	virtual void onEnter();
    
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* scene();
    
    // implement the "static node()" method manually
    CREATE_FUNC(GameScene);
    
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

    Darkness* getDark() { return dark_;}
	cocos2d::TMXTiledMap* getMap() { return map_; }
};
#endif /* defined(__HelloCpp__GameScene__) */
