//
//  Platform.h
//  Lost Sunrise
//
//  Created by Roman on 5/8/13.
//
//

#ifndef __Lost_Sunrise__Platform__
#define __Lost_Sunrise__Platform__

#include "cocos2d.h"
#include "Box2D/Box2D.h"

USING_NS_CC;

class Platform : public Node
{
public:
    ~Platform();
    Platform();
    
    bool init(b2World* world, ValueMap properties);
    b2Body *body;
};
#endif /* defined(__Lost_Sunrise__Platform__) */
