//
//  Player.h
//  HelloCpp
//
//  Created by Roman on 26.04.14.
//
//

#ifndef __HelloCpp__Player__
#define __HelloCpp__Player__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Constants.h"
#include "LightSource.h"

class Player : public cocos2d::Sprite
{
    void updateStep();
    void setIdle();
    
    PlayerState state_;
    b2Body* body_;
    b2World* world_;
    
	LightSource* light_;

    cocos2d::Action* curAction_;
    cocos2d::Animate *shootAnim_;

    bool pressRight_;
    bool pressLeft_;
    bool pressUp_;
    bool pressDown_;
    
	bool jump_;
    int stepCount_;
    float lastStepPosX;

	int bullets;
	int zippos;
	int grenades;

protected:
    void setPosition(const cocos2d::Point &pos);

public:
	void reset();
    void update(float delta);
    bool isFly();
    void setState(PlayerState state);
    b2Body* body();
	LightSource* light() { return light_; }
    int getGID();
    
    Player();
    void stop();
    virtual ~Player();
    
	PlayerState getState();
    bool init(b2World* world);
    void shoot();
    
    void onLeft(bool isPush);
    void onRight(bool isPush);
    void onJump(bool isPush);
    
    void onZippo();
    void onGrenade();
	bool isJump() { return jump_; }
	void setJump(bool jump) { jump_ = jump; }
};
#endif /* defined(__HelloCpp__Player__) */
