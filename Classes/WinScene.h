//
//  WinScene.h
//  ld29
//
//  Created by Roman on 29.04.14.
//
//

#ifndef __ld29__WinScene__
#define __ld29__WinScene__

#include "cocos2d.h"
#include "cocos-ext.h"

class WinScene : public cocos2d::Layer
{
public:
    void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    CREATE_FUNC(WinScene);
};

#endif /* defined(__ld29__WinScene__) */
