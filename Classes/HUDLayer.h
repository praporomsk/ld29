//
//  HUDLayer.h
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#ifndef __ld29__HUDLayer__
#define __ld29__HUDLayer__

#include "cocos2d.h"

class HUDLayer : public cocos2d::Layer
{
    void onZippo();

	cocos2d::MenuItemSprite* zippoItem;
	cocos2d::MenuItemSprite* shItem;
	cocos2d::MenuItemSprite* gunItem;

    void addAmmo();
    
    cocos2d::Sprite* ammoBar;
public:
    bool init();

	void setGun(int count);
	void setGrenade(int count);
	void setZippo(int count);
};
#endif /* defined(__ld29__HUDLayer__) */
