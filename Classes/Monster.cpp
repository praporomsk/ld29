//
//  Monster.cpp
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#include "Monster.h"

USING_NS_CC;

Monster::Monster()
{
    body_ = NULL;
    eyes_ = NULL;
}


Monster::~Monster()
{
    CC_SAFE_RELEASE(eyes_);
}

bool Monster::init(b2World* world, const Point& pos)
{
    if(!Sprite::initWithSpriteFrameName("enemyNorm_0.png"))
        return false;
    
    setTag(BodyEnemy);
    getTexture()->setAliasTexParameters();
    setScale(GLOBAL_SCALE);
    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    
    body_ = world->CreateBody(&bodyDef);
    
    // Define another box shape for our dynamic body.
    
    b2PolygonShape box;
    
    box.SetAsBox((boundingBox().size.width/2.0)  / PTM_RATIO, (boundingBox().size.height/4.0) / PTM_RATIO);

    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &box;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.0f;
    fixtureDef.restitution = 0.0f;
    fixtureDef.filter.categoryBits = CATEGORY_ENEMY;
    fixtureDef.filter.maskBits = MASK_ENEMY;
    
    //fixtureDef.isSensor = true;
    body_->CreateFixture(&fixtureDef);
    body_->SetUserData(this);
    
    Vector<cocos2d::AnimationFrame *> spriteFrames;
    SpriteFrameCache* cache = SpriteFrameCache::getInstance();
    
    for(int i = 0; i < 8; i++)
    {
        static char buff[64];
        sprintf(buff, "enemyNorm_%d.png", i);
        ValueMap userInfo;
        AnimationFrame* animFrame = AnimationFrame::create(cache->getSpriteFrameByName(buff), 0.2, userInfo);
        spriteFrames.pushBack(animFrame);
    }
    
    runAction(RepeatForever::create(Animate::create(Animation::create(spriteFrames, 0.6))));
    
    
    eyes_ = Sprite::createWithSpriteFrameName("enemyInDark_0.png");
    CC_SAFE_RETAIN(eyes_);
    
    Vector<cocos2d::AnimationFrame *> eyeFrames;
 
    for(int i = 0; i < 8; i++)
    {
        static char buff[64];
        sprintf(buff, "enemyInDark_%d.png", i);
        ValueMap userInfo;
        AnimationFrame* animFrame = AnimationFrame::create(cache->getSpriteFrameByName(buff), 0.2, userInfo);
        eyeFrames.pushBack(animFrame);
    }
    
    eyes_->runAction(RepeatForever::create(Animate::create(Animation::create(eyeFrames, 0.6))));
    //eyes_->retain();
    
    //scheduleOnce(schedule_selector(Shell::kill), 2.0);
    scheduleUpdate();
    return true;
}



void Monster::setPosition(const cocos2d::Point &pos)
{
    Sprite::setPosition(pos);
    eyes_->setPosition(Point(pos.x - 80, pos.y));
}

void Monster::update(float delta)
{
    b2Vec2 pos = body_->GetPosition();
    body_->SetTransform(b2Vec2(pos.x - delta * 4, pos.y), 0);
    
    if (getPosition().x < 0)
        setTag(NeedDestroyBody);
    
}

void Monster::kill()
{
    eyes_->removeFromParentAndCleanup(true);
    setTag(NeedDestroyBody);
}