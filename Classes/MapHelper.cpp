//
//  MapHelper.cpp
//  Rabbit
//
//  Created by Roman on 4/18/13.
//
//

#include "MapHelper.h"
#include "GameScene.h"

PointArray* MapHelper::parseDictionary(ValueMap properties)
{
    
    Point startPos = Point(properties["x"].asFloat(), properties["y"].asFloat());
    startPos = startPos * 1/ CCDirector::getInstance()->getContentScaleFactor();
    //ccpMult(startPos, 1/ CCDirector::sharedDirector()->getContentScaleFactor());

    PointArray *points = new PointArray();
    points->initWithCapacity(1);
    points->autorelease();
    points->addControlPoint(startPos);

	//startPos.y = MAP_HEIGHT - startPos.y;

    ValueVector polylinePoints = properties["polylinePoints"].asValueVector();
    
    for (int i = 0; i < polylinePoints.size(); i++) {
        ValueMap val = polylinePoints[i].asValueMap();
		Point sPos = Point(val["x"].asFloat(), val["y"].asFloat());
        points->addControlPoint(startPos +  Point(sPos.x, -sPos.y) );
    }
    return points;
}

Point MapHelper::positionWithProperties(ValueMap properties)
{
    return Point(properties["x"].asFloat(), properties["y"].asFloat());
}