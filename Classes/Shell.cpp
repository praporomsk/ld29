//
//  Shell.cpp
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#include "Shell.h"
USING_NS_CC;

Shell::Shell()
{
    body_ = NULL;
}


Shell::~Shell()
{
    
}

bool Shell::init(b2World* world, const Point& pos)
{
    if(!Sprite::initWithFile("shell.png"))
        return false;
    
    getTexture()->setAliasTexParameters();
    setScale(GLOBAL_SCALE);
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(pos.x/PTM_RATIO, pos.y/PTM_RATIO);
    
    body_ = world->CreateBody(&bodyDef);
    
    // Define another box shape for our dynamic body.
    
    b2PolygonShape box;
    
    box.SetAsBox((boundingBox().size.width/2.0)  / PTM_RATIO, (boundingBox().size.height/2.0) / PTM_RATIO);
    //fixtureDef.shape = &box;
    
    //    b2CircleShape *circleShape = new b2CircleShape();
    //    circleShape->m_radius = ((boundingBox().size.width/2) / PTM_RATIO) * getScale();
    
    // Define the dynamic body fixture.
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &box;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.2f;
    fixtureDef.restitution = 0.0f;
    fixtureDef.filter.categoryBits = CATEGORY_SHELL;
    fixtureDef.filter.maskBits = MASK_SHELL;
    
    body_->CreateFixture(&fixtureDef);
    body_->SetUserData(this);
    
    body_->SetAngularVelocity(10 + randomNumber(15));

    body_->ApplyForceToCenter(b2Vec2(-5, 5), false);

    scheduleOnce(schedule_selector(Shell::kill), 2.0);
    //scheduleUpdate();
    return true;
}




//void Shell::update(float delta)
//{
//    angle_ -= angleDelta_;
//    body_->SetTransform(body_->GetPosition(), angle_);
//}

void Shell::kill(float dt)
{
    setTag(NeedDestroyBody);
}