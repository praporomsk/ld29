//
//  Bullet.h
//  ld29
//
//  Created by Roman on 28.04.14.
//
//

#ifndef __ld29__Bullet__
#define __ld29__Bullet__

#include "cocos2d.h"
#include "Box2D/Box2D.h"
#include "Constants.h"
#include "LightSource.h"

class Bullet : public cocos2d::Sprite
{
    
    b2Body* body_;
    LightSource* light_;
    bool active_;
    
protected:
    void setPosition(const cocos2d::Point &pos);
    
public:
    void setActive(bool active);
    bool active() { return active_; }
    
    b2Body* body();
    void kill(float dt);
    Bullet();
    virtual ~Bullet();
    
    bool init(b2World* world, const cocos2d::Point& pos);
    
    cocos2d::MotionStreak* streak;
};

#endif /* defined(__ld29__Bullet__) */
