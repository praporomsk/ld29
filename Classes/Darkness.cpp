#include "Darkness.h"
#include "Constants.h"
#include "GameScene.h"

USING_NS_CC;

Darkness::Darkness(int w, int h)
{
	winSize = CCDirector::getInstance()->getWinSize();
	darkSize = Size(w, h);
   

	light = Sprite::create("light.png");
	light->retain();

	render = CCRenderTexture::create(winSize.width, h);//(w, h);	
	addChild(render);
	render->setPosition(Point(winSize.width / 2.0, h / 2.0));
    
	lights = CCArray::create();
	lights->retain();

	Scene* scene = CCDirector::getInstance()->getRunningScene();
	GameScene* game = (GameScene*)scene->getChildByTag(99);
	player = game->getPlayer();
}

Darkness::~Darkness()
{
	Ref* obj = NULL;
	CCARRAY_FOREACH(lights, obj)
	{
		LightSource* source = (LightSource*)obj;
		CCDirector::sharedDirector()->getScheduler()->unscheduleAllForTarget(source);
	}

	lights->release();
}

void Darkness::addLightSource(LightSource* ls)
{
	lights->addObject(ls);
}

void Darkness::removeLightSource(LightSource* ls)
{
	lights->removeObject(ls);
}

void Darkness::visit(Renderer* renderer, const kmMat4 &parentTransform, bool parentTransformUpdated)
{
	Point playerPos = player->getPosition();
	float x = 0;
	if (playerPos.x < winSize.width / 2.0)
		x = winSize.width / 2.0 - playerPos.x;
	else if (playerPos.x > darkSize.width - winSize.width / 2.0)
		x = -winSize.width / 2.0 + (darkSize.width - playerPos.x);

	render->beginWithClear(0, 0, 0, 255);

	Ref* obj = NULL;
	CCARRAY_FOREACH(lights, obj)
	{
		LightSource* source = (LightSource*)obj;
		Point pos = source->getPosition();
		Sprite* l = Sprite::create("light.png");
        if (source->getType() == LS_BIG) {
            l->setScale(1.0);
        }else if (source->getType() == LS_BULLET){
            l->setOpacity(100);
            l->setScale(0.3);
        } else if (source->getType() == LS_SIGA){
            l->setOpacity(150);
            l->setScale(0.03);
		}
		else if (source->getType() == LS_GRENADE){
			l->setScale(1);
		}
		else if (source->getType() == LS_SHOT){
			l->setOpacity(255);
			l->setScale(100);
        }else{
            l->setScale(1);
        }
		l->setBlendFunc({ GL_ZERO, GL_ONE_MINUS_SRC_ALPHA });
		l->setPosition(x > 0.0001 ? pos.x : winSize.width / 2.0 + (x < -0.0001 ? -x : 0) + pos.x - playerPos.x/* darkSize.width / winSize.width*/, pos.y * darkSize.height / winSize.height);

		l->visit();
	}

	render->end();

	render->setPosition(playerPos.x + x, darkSize.height / 2.0);

	render->visit(renderer, parentTransform, parentTransformUpdated);
}